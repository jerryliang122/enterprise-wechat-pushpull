from flask import request, jsonify
from flask import Flask
import json,requests
from flask.globals import g
import wechenpush
from WXBizMsgCrypt3 import WXBizMsgCrypt
import xml.etree.cElementTree as ET
import sys
import os

#获取get参数 并且发送到企业微信
push = Flask(__name__)
wxcpt=WXBizMsgCrypt('sToken','sEncodingAESKey','sCorpID')
#请记得填写上方的参数在API中
#这里是标准的企业微信上载转发类似方糖
@push.route('/push.php', methods = ["GET","POST"])   # GET 和 POST 都可以
def get_data():
    # 假设有如下 URL
    # http://10.8.54.48:5000/index?name=john&age=20

    #可以通过 request 的 args 属性来获取参数
    wecom_touid = request.args.get('wecom_touid')
    wecom_aid = request.args.get('wecom_aid')
    text = request.args.get("title")
    wechenpush.push(text)
    return text
#这个使用在第一次进行验证企业微信确认，确认后即可注释
@push.route('/cgi-bin/', methods = ["GET","POST"])
def get_pull():
    msg_signature = request.args.get("msg_signature")
    timestamp = request.args.get('timestamp')
    nonce = request.args.get('nonce')
    echostr = request.args.get('echostr')
    ret,sEchoStr=wxcpt.VerifyURL(msg_signature, timestamp,nonce,echostr)
    return sEchoStr
#在进行验证时请删除该段代码，微信接收消息api
@push.route('/cgi-bin', methods = ["GET","POST"])
def wechen_pull():
    msg_signature = request.args.get('msg_signature')
    timestamp = request.args.get('timestamp')
    nonce = request.args.get("nonce")
    sReqData = request.get_data()
    ret,sMsg=wxcpt.DecryptMsg( sReqData, msg_signature, timestamp, nonce)
    if( ret!=0 ):
        print( "ERR: DecryptMsg ret: " + str(ret))
        sys.exit(1)
    xml_tree = ET.fromstring(sMsg)
    content = xml_tree.find("Content").text
    if 'temp' in content:
        file = open('temp.txt').read()
        wechenpush.push(file)
    return file
#温湿度传感器上报
@push.route('/pushtemp', methods = ["GET","POST"])
def temp():
    humidity = request.args.get('humidity')
    temperature = request.args.get('temperature')
    file = open('temp.txt','w+')
    file.write('湿度:'+str(humidity)+'%,'+'温度:'+str(temperature)+'C')
    file.close()
    a ='ok'
    return a
if __name__ == '__main__':
    push.run(port = 8081,debug=True)