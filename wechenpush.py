import json,requests
import datetime
import os
def push(text):
    #获取当前时间戳
    time = datetime.datetime.now().timestamp()
    time = int(time)
    info =os.path.exists('access_token')
    if not info:
        change = 7300
    else:
        filetime = os.stat('access_token')
        filemtime = int(filetime.st_ctime)
        change = time - filemtime
    wecom_cid = ''
    wecom_secret = ''
    wecom_aid = ''
    wecom_touid = ''
    #这里输入你的应用信息,请注意，在第一次使用该脚本时，由于下载因素会导致access_token的创建时间小于7200秒不触发获取token
    #判定文件是否过期2小时
    if change >= 7200:
        get_token_url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={wecom_cid}&corpsecret={wecom_secret}"
        response = requests.get(get_token_url).content
        access_token = json.loads(response).get('access_token')
        if info is True: 
            os.remove('access_token')
        file = open('access_token','w+')
        file.write(access_token)
        file.close()
    else:
        access_token = open('access_token').read()
    send_msg_url = f'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={access_token}'
    data = {
        "touser":wecom_touid,
        "agentid":wecom_aid,
        "msgtype":"text",
        "text":{
            "content":text
        },
        "duplicate_check_interval":600
    }
    response = requests.post(send_msg_url,data=json.dumps(data)).content
    